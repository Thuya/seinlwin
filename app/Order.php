<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
    	'user_id',
    	'payment',
    	'order_date',
    	'order_month',
    	'address',
    	'total_quantity',
    	'total_amount',
    ];
    public function deliveries()
    {
    	return $this->hasMany('App\Delivery');
    }
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function orderdetails()
    {
        return $this->hasMany('App\OrderDetail');
    }
}
