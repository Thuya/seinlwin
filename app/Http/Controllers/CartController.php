<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Carbon\Carbon;

class CartController extends Controller
{
    public function addtocart($id, Request $request)
    {
    	$product = Product::find($id);

    	if (!$product) {
    		abort(404);
    	}
    	$cart = session()->get('cart');
    	if (!$cart) {
    		$cart = [
    			$id => [
    				'name' => $product->name,
    				'totalqty' => $product->totalqty,
    				'quantity' => $request->quantity,
    				'price' => $product->price,
                    'color' => $request->color,
    				'photo' => $product->productimages[0]->image
    			]
    		];

    		session()->put('cart', $cart);
    		return redirect()->back()->with('success', 'Product added to cart successfully!');
    	}

    	if (isset($cart[$id])) {
    		$cart[$id]['quantity']++;
    		session()->put('cart', $cart);
    		return redirect()->back()->with('success', 'Product added to cart successfully!');
    	}

    	$cart[$id] = [
    		'name' => $product->name,
    		'totalqty' => $product->totalqty,
    		'quantity' => $request->quantity,
    		'price' => $product->price,
            'color' => $request->color,
    		'photo' => $product->productimages[0]->image
    	];
    	session()->put('cart', $cart);
    	return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function update(Request $request)
    {
        if ( $request->id && $request->quantity ) {

            $cart = session()->get('cart');

            $cart[$request->id]['quantity'] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');

            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }
            session()->flash('success', 'Cart removed successfully');
        }
    }

    public function allcarts()
    {
        $month = Carbon::now()->month;
        return view('/users/cart', [
            'month' => $month
        ]);
    }
}
