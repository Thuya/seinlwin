<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use PDF;
use App\Order;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function index()
    {
    	$reports = Product::all();
    	return view('admin.reports.index', [
    		'reports' => $reports
    	]);
    }

    public function reportall()
    {
    	$reports = Order::all();
    	$pdf = PDF::loadView('admin.reports.report', [ 'reports' => $reports ]);
        $pdf->setPaper('A4', 'landscape');
       	return $pdf->download('reports.pdf');
    }

    public function daily()
    {
    	$today = Carbon::now()->toDateString();
    	$reports = Order::where('order_date', $today)->get();
    	$pdf = PDF::loadView('admin.reports.report', [ 'reports' => $reports ]);
        $pdf->setPaper('A4', 'landscape');
       	return $pdf->download('reports-daily.pdf');
    }
    public function monthly()
    {
        $month = Carbon::now()->month;
        $reports = Order::where('order_month', $month)->get();
        $pdf = PDF::loadView('admin.reports.report', [ 'reports' => $reports ]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('reports-monthly.pdf');
    }
}
