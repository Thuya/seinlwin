<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\Review;
use App\User;
use Auth;
use Carbon\Carbon;

class UserController extends Controller
{
	public function search(Request $request)
	{
	    $product_search = $request->search;
	    $products = Product::where('name', 'LIKE', '%' . $product_search . '%') 
        ->orWhereHas('category', function($q) use ($product_search) {
            return $q->where('name', 'LIKE', '%' . $product_search . '%');
        })
        ->orWhereHas('brand', function($q) use ($product_search) {
            return $q->where('name', 'LIKE', '%'. $product_search . '%');
        })->get();

	    return view('users.all', [ 'products' => $products ]);
	}

    public function genderproducts($gender)
    {
    	$products = Product::where('gender', $gender)->get();
    	return view('users.humantypeproduct', [ 'products' => $products ]);
    }

    public function journal()
    {
    	$products = Product::take(5)->orderBy('id', 'desc')->get();
    	return view('users.journal', [ 'products' => $products ]);
    }

    public function detail($id)
    {
    	$product = Product::find($id);
        // $reviews = Review::where('product_id', $id)->where('created_at', '<', Carbon::now()->subMinutes(5)->toDateTimeString())->get();
        // dd($reviews);
    	return view('users.detail', [ 'product' => $product ]);
    }

    public function typeproducts($type)
    {
        $products = Product::where('product_type', $type)->get();
        // dd($products);
        return view('users.typeproduct', [ 'products' => $products ]);
    }

    public function history()
    {
        $orders = Order::where('user_id', Auth::user()->id)->get();
        // dd($products);
        return view('users.history', [ 'orders' => $orders ]);
    }

    public function filter(Request $request)
    {
        // dd($request);
        $result = $request->filter;
        if ( $result == 'low' ) {
            $products = Product::where('gender', $request->gender)
                        ->orderBy('price')
                        ->get();
        } elseif ( $result == 'high' ) {
            $products = Product::where('gender', $request->gender)
                        ->orderBy('price', 'desc')
                        ->get();
        }
        return view("users.humantypeproduct", [
            'products' => $products
        ]);
    }
    public function typefilter(Request $request)
    {
        $result = $request->filter;
        if ( $result == 'low' ) {
            $products = Product::where('product_type', $request->type)
                        ->orderBy('price')
                        ->get();
        } elseif ( $result == 'high' ) {
            $products = Product::where('product_type', $request->type)
                        ->orderBy('price', 'desc')
                        ->get();
        }
        return view("users.typeproduct", [
            'products' => $products
        ]);
    }
}
