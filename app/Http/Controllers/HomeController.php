<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function account()
    {
        return view('admin.account');
    }
    public function accupdate(Request $request, $id)
    {
        if ($request->old_pw == null || $request->new_pw == null) {
           session()->flash('danger', 'Password fields must not be null!');
            return redirect()->back();
        }
        if (!(Hash::check($request->old_pw, Auth::user()->password))) {
            session()->flash('danger', 'Your old password is incorrect!');
            return redirect()->back();
        }
        if (strcmp($request->old_pw, $request->new_pw) == 0) {
            session()->flash('danger', 'Your old password and new password must not be same!');
            return redirect()->back();
        }
        $user = Auth::user();
        $user->update([
            'name' => $request->name,
            'password' => Hash::make($request->new_pw),
        ]);
        session()->flash('success', 'Account updated successfully!');
        return redirect()->back();
    }
}
