<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class PurchaseController extends Controller
{
    public function create($id)
    {
    	$purchase = Product::find($id);
    	return view('admin.purchases.purchase', [
    		'purchase' => $purchase
    	]);	
    }

    public function store(Request $request, $id)
    {
    	$purchase = Product::find($id);
    	$plus_quantity = $purchase->totalqty + $request->totalqty;
    	$purchase->update([
    		'totalqty' => $plus_quantity,
    	]);
    	return redirect('/admin/reports')->with('success', 'Product purchase successfully!');
    }
}
