<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;
use Validator;

class ColorController extends Controller
{
    public function index()
	{
		$colors = Color::all();
		return view('admin/colors/index', ['colors' => $colors]);
	}

    public function create()
    {
    	return view('admin/colors/create');
    }

    public function store(Request $request)
    {
    	$messages = [
    		'name.required' => 'Need to fill this field!',
    	];
    	$validator = Validator::make($request->all(), [
            'name' => 'required|unique:colors',
        ], $messages)->validate();
    	Color::create($request->all());
    	return redirect('/admin/colors')
    	->with('message', 'Your colors is successfully created!')
    	->with('status', 'success');
    }

    public function edit($id)
    {
    	$color = Color::find($id);
    	return view('/admin/colors/edit', [ 'color' => $color ]);
    }

    public function update(Request $request, $id)
    {
    	$color = Color::find($id);
    	$color->update($request->all());
    	return redirect('/admin/colors')
    	->with('message', 'Your colors is successfully updated!')
    	->with('status', 'warning');
    }

    public function delete($id)
    {
    	$color = Color::find($id);
    	$color->delete();

    	return redirect('/admin/colors')
    	->with('message', 'Your colors is successfully deleted!')
    	->with('status', 'danger');
    }
}
