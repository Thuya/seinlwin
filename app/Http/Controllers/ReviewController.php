<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use Auth;

class ReviewController extends Controller
{
    public function store(Request $request)
    {
    	Review::create([
    		'review' => $request->review,
    		'product_id' => $request->product_id,
    		'user_id' => Auth::user()->id
    	]);

    	return redirect()->back()->with('success', 'Thank You for your review!');
    }
}
