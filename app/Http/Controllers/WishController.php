<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class WishController extends Controller
{
    public function wish($id)
    {
    	$product = Product::find($id);
    	// dd($product);
    	if (! $product) {
    		abort(404);
    	}
    	$wish = session()->get('wish');
    	if (! $wish) {
    		$wish = [
    			$id => [
    				'id' => $product->id,
    				'name' => $product->name,
    				'totalqty' => $product->totalqty,
    				'price' => $product->price,
    				'photo' => $product->productimages[0]->image
    			]
    		];
    		session()->put('wish', $wish);
    		return redirect()->back()->with('success', 'Product added to wish list!');
    	}
    	$wish[$id] = [
    		'name' => $product->name,
    		'totalqty' => $product->totalqty,
    		'price' => $product->price,
    		'photo' => $product->productimages[0]->image
    	];
    	$wishlist = session()->put('wish', $wish);
    	return redirect()->back()->with('success', 'Product added to wish list!');
    }
    public function allwish()
    {
        return view('/users/wish');
    }

    public function wishremove($id)
    {
    	if ($id) {
    		$wish = session()->get('wish');
    		if(isset($wish[$id])) {

                unset($wish[$id]);

                session()->put('wish', $wish);
            }
            return redirect()->back()->with('success', 'Product removed from wish list!');
    	}
    }
}
