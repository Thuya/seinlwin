<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brand;
use App\Color;
use App\ColorProduct;
use App\ProductSize;
use App\ProductImage;
use DB;
use Validator;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->get();
        return view('admin/products/index', ['products' => $products]);
    }
    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        $colors = Color::all();
        return view('admin/products/create', [
            'categories' => $categories,
            'brands' => $brands,
            'colors' => $colors
        ]);
    }

    public function store(Request $request)
    {
        // dd($request);
        $messages = [
            'name.required' => 'Need to fill this field!',
            'price.required' => 'Need to fill this field!',
            'code.required' => 'Need to fill this field!',
            'description.required' => 'Need to fill this field!',
            'totalqty.required' => 'Need to fill this field!',
            'images.required' => 'Need to fill this field!',
            'color_id.required' => 'Need to fill this field!',
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'code' => 'required',
            'description' => 'required',
            'totalqty' => 'required',
            'images' => 'required',
            'color_id' => 'required',
        ], $messages)->validate();
        // dd($request);
        $colors = $request->color_id;
        $images = $request->file('images');
        // dd($images);
        $product = Product::create($request->all());

        foreach($colors as $color) {
            ColorProduct::create([
                'product_id' => $product->id,
                'color_id' => $color
            ]);
        }
        foreach ($images as $image) {
            // dd($image);
            $file = $image;
            $path = public_path().'/uploads/';
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);
            ProductImage::create([
                'image' => 'uploads/' . $filename,
                'product_id' => $product->id
            ]);
        }
        return redirect('/admin/products')
        ->with('message', 'Product is created and fill again product varient!')
        ->with('status', 'success');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        $colors = Color::all();
        $selected_color = DB::table('color_products')
                        ->where('product_id', $product->id)
                        ->pluck('color_id')
                        ->all();
        $brands = Brand::all();
        return view('/admin/products/edit', [ 
            'product' => $product,
            'categories' => $categories,
            'brands' => $brands,
            'colors' => $colors,
            'selected_color' => $selected_color
        ]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $colors = $request->color_id;
        $product->update($request->all());

        $colorproduct = ColorProduct::where('product_id', $product->id)->delete();
        foreach($colors as $color) {
            ColorProduct::create([
                'product_id' => $product->id,
                'color_id' => $color
            ]);
        }
        return redirect('/admin/products')
        ->with('message', 'Your product is successfully updated!')
        ->with('status', 'warning');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('/admin/products/show', [ 'product' => $product ]);
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $colorproduct = ColorProduct::where('product_id', $product->id)->delete();
        $product->delete();

        return redirect('/admin/products')
        ->with('message', 'Your product is successfully deleted!')
        ->with('status', 'danger');
    }

    public function phone($id)
    {
    	$phones = Product::with('category')->where('category_id', $id)->get();
        $category = Category::find($id);
    	return view('userview/phone', [ 'phones' => $phones, 'category' => $category ]);
    }

    public function allproducts()
    {
        $phones = Product::all();
        
        return view('userview/phone', [ 'phones' => $phones ]);
    }

    public function detail($id)
    {
        $product = Product::find($id);
        return view('userview/detail', ['product' => $product]);
    }

    public function adminsearch(Request $request)
    {
        $product_search = $request->search;
        // $products = Product::where('name', 'LIKE', '%' . $product_search . '%')->get();
        $products = Product::where('name', 'LIKE', '%' . $product_search . '%') 
        ->orWhereHas('category', function($q) use ($product_search) {
            return $q->where('name', 'LIKE', '%' . $product_search . '%');
        })
        ->orWhereHas('brand', function($q) use ($product_search) {
            return $q->where('name', 'LIKE', '%'. $product_search . '%');
        })->get();

        return view('/admin/products/index', [ 'products' => $products ]);
    }
}
