<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Help;

class HelpController extends Controller
{
    public function index()
    {
    	return view('users.help');
    }

    public function store(Request $request)
    {
    	// dd($request);
    	Help::create($request->all());
    	return redirect()->back();
    }

    public function adminview()
    {
    	$helps = Help::all();
    	return view('admin.help', [
    		'helps' => $helps
    	]);
    }

    public function delete($help)
    {
    	$help = Help::find($help);
    	$help->delete();
    	return redirect()->back();
    }
}
