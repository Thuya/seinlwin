<?php

namespace App\Http\Controllers;
use App\Order;
use App\OrderDetail;
use App\Product;
use Validator;
use Mail;
use PDF;
use Auth;
use App\Delivery;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function checkout(Request $request)
    {
      // dd($request);
   //  	$messages = [
   //  		'name.required' => 'Need to fill this field!',
   //  		'total_quantity' => 'Need to fill this field!',
			// 'total_amount' => 'Need to fill this field!',
			// 'note' => 'Need to fill this field!'
   //  	];
   //  	$validator = Validator::make($request->all(), [
   //          'name' => 'required|unique:categories',
   //          'total_quantity' => 'required',
   //          'total_amount' => 'required',
   //          'note' => 'required',
   //      ], $messages)->validate();

        $carts = session()->get('cart');
        $order = Order::create($request->all());
        $delivery = Delivery::create([
            'fees' => $request->total_amount,
            'date' => $request->order_date,
            'contact_person' => $request->contact_person,
            'phone' => $request->phone,
            'address' => $request->address,
            'order_id' => $order->id
        ]);

        foreach ($carts as $cart => $value) {
        	$orderdetail = OrderDetail::create([
		    	'order_id' => $order->id,
  				'product_id' => $cart,
  				'product_name' => $value['name'],
  				'color' => $value['color'],
  				'quantity' => $value['quantity'],
  				'price' => $value['price'],
        	]);
            $product = Product::find($cart);
            $minusqty = $product->totalqty - $orderdetail->quantity;
            $product->update([
                'totalqty' => $minusqty,
            ]);
        }

        session()->forget('cart');
        $pdf = PDF::loadView('pdf.pdf', [ 'order' => $order ]);
        $pdf->setPaper('A4', 'landscape');
        Mail::raw('Thank You for buying products.', function($message) use($pdf)
        {
            $message->subject('DT Hypebeaset');
            $message->from('no-reply@DTH.com', 'SeinLwin');
            $message->to('sanlwingod@gmail.com');
            $message->attachData($pdf->output(), "confirmation.pdf");
        }); 
        return redirect('/confirm/products/order=' . $order->id);
    }

    public function orderproduct($id)
    {
        $buy = Order::find($id);
        $buys = Order::find($id)->orderdetails;
        return view('users.confirmproduct', ['buy' => $buy, 'buys' => $buys]);
    }

      public function index()
      {
        $orders = Order::all();
        return view('admin.orders.index', [ 'orders' => $orders ]);
      }
      public function history()
      {
        $orders = Order::with('user', 'orderdetails')->where('user_id', Auth::user()->id)->get();
        return view('users.history', [ 'orders' => $orders ]);
      }
}
