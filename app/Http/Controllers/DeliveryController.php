<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delivery;

class DeliveryController extends Controller
{
    public function index()
    {
    	$deliveries = Delivery::all();
    	return view('admin.deliveries.index', [ 'deliveries' => $deliveries ]);
    }

    public function status($id)
    {
    	$delivery = Delivery::find($id);
    	$delivery->update([
    		'status' => 'completed'
    	]);
    	return redirect()->back();
    }
}
