<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
    	'name',
    	'description',
    	'price',
    	'code',
    	'gender',
        'product_type',
    	'totalqty',
    	'category_id',
    	'brand_id',
    ];
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
    public function brand()
    {
    	return $this->belongsTo('App\Brand');
    }
    public function colorproducts()
    {
    	return $this->hasMany('App\ColorProduct');
    }
    public function orderdetails()
    {
    	return $this->hasMany('App\OrderDetail');
    }
    public function productimages()
    {
    	return $this->hasMany('App\ProductImage');
    }
    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
}
