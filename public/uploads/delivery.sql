-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 31, 2017 at 07:56 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `diamond_plus_restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE IF NOT EXISTS `delivery` (
  `DeliveryID` varchar(30) NOT NULL,
  `DeliveryDate` varchar(20) NOT NULL,
  `DeliveryTime` varchar(45) NOT NULL,
  `Deliveryaddress` varchar(155) NOT NULL,
  `StaffID` varchar(30) NOT NULL,
  `Car` varchar(55) NOT NULL,
  PRIMARY KEY (`DeliveryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery`
--

INSERT INTO `delivery` (`DeliveryID`, `DeliveryDate`, `DeliveryTime`, `Deliveryaddress`, `StaffID`, `Car`) VALUES
('D_000001', 'Jul-21-2017', '6:00', 'Yann', 'S_000002', '5E/5555'),
('D_000002', 'Jul-24-2017', '6:00', 'SanChoung', 'S_000002', '9L/9999'),
('D_000003', '26-Jul-2017', '5:00', 'Yangon', 'S_000003', '5E/5555');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
