<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/help', 'HelpController@index')->name('help');
Route::post('/help/store', 'HelpController@store')->name('help.store');

Route::group([ 'middleware' => ['auth','admin']], function()
{
	Route::get('/admin/products', 'ProductController@index')->name('admin.products');
	Route::post('/admin/products/search', 'ProductController@adminsearch')->name('admin.search');
	Route::get('/admin/products/create', 'ProductController@create')->name('products.create');
	Route::get('/admin/products/varients/create/id={id}', 'ProductController@varient')->name('products.varient');
	Route::post('/admin/products/varients/store/id={id}', 'ProductController@varientstore')->name('varient.store');
	Route::post('/admin/products/store', 'ProductController@store')->name('products.store');
	Route::get('/admin/products/edit/id={id}', 'ProductController@edit')->name('products.edit');
	Route::post('/admin/products/update/id={id}', 'ProductController@update')->name('products.update');
	Route::get('/admin/products/show/id={id}', 'ProductController@show')->name('products.show');
	Route::get('/admin/products/delete/id={id}', 'ProductController@delete')->name('products.delete');

	Route::get('/admin/categories', 'CategoryController@index')->name('admin.categories');
	Route::get('/admin/categories/create', 'CategoryController@create')->name('categories.create');
	Route::post('/admin/categories/store', 'CategoryController@store')->name('categories.store');
	Route::get('/admin/categories/edit/id={id}', 'CategoryController@edit')->name('categories.edit');
	Route::post('/admin/categories/update/id={id}', 'CategoryController@update')->name('categories.update');
	Route::get('/admin/categories/delete/id={id}', 'CategoryController@delete')->name('categories.delete');

	Route::get('/admin/brands', 'BrandController@index')->name('admin.brands');
	Route::get('/admin/brands/create', 'BrandController@create')->name('brands.create');
	Route::post('/admin/brands/store', 'BrandController@store')->name('brands.store');
	Route::get('/admin/brands/edit/id={id}', 'BrandController@edit')->name('brands.edit');
	Route::post('/admin/brands/update/id={id}', 'BrandController@update')->name('brands.update');
	Route::get('/admin/brands/delete/id={id}', 'BrandController@delete')->name('brands.delete');

	Route::get('/admin/colors', 'ColorController@index')->name('admin.colors');
	Route::get('/admin/colors/create', 'ColorController@create')->name('colors.create');
	Route::post('/admin/colors/store', 'ColorController@store')->name('colors.store');
	Route::get('/admin/colors/edit/id={id}', 'ColorController@edit')->name('colors.edit');
	Route::post('/admin/colors/update/id={id}', 'ColorController@update')->name('colors.update');
	Route::get('/admin/colors/delete/id={id}', 'ColorController@delete')->name('colors.delete');

	Route::get('/admin/orders/all', 'OrderController@index')->name('orders.all');
	Route::get('/admin/deliveries/all', 'DeliveryController@index')->name('deliveries');
	Route::get('/admin/deliveries/status/id={id}', 'DeliveryController@status')->name('delivery.status');

	Route::get('/admin/reports/generate/all', 'ReportController@reportall')->name('reports.all');
	Route::get('/admin/reports/daily', 'ReportController@daily')->name('reports.daily');
	Route::get('/admin/reports/monthly', 'ReportController@monthly')->name('reports.monthly');
	Route::get('/admin/manage/account', 'HomeController@account')->name('admin.account');
	Route::post('/admin/manage/account/update/id={id}', 'HomeController@accupdate')->name('admin.accupdate');
	Route::get('/admin/reports', 'ReportController@index')->name('reports');
	Route::get('/admin/purchases/product={id}', 'PurchaseController@create')->name('purchases');
	Route::post('/admin/purchases/update/product={id}', 'PurchaseController@store')->name('purchases.create');
	Route::get('/admin/deliveries', 'DeliveryController@index')->name('admin.delivery');
	Route::get('/admin/deliveries/update/status={id}', 'DeliveryController@status')->name('delivery.status');
	Route::get('/admin/customer/messages', 'HelpController@adminview')->name('admin.help');
	Route::get('/admin/customer/messages/delete={help}', 'HelpController@delete')->name('help.delete');
});

Route::group([ 'middleware' => ['auth'] ], function()
{
	Route::get('/products/gender={gender}', 'UserController@genderproducts')->name('products.gender');
	Route::get('/products/type={type}', 'UserController@typeproducts')->name('products.type');
	Route::get('/journal', 'UserController@journal')->name('journal');
	Route::get('/products/detail/id={id}', 'UserController@detail')->name('product.detail');
	Route::post('/products/search', 'UserController@search')->name('products.search');
	Route::post('/products/filter', 'UserController@filter');
	Route::post('products/type/filter', 'UserController@typefilter');

	Route::get('/wish/', 'WishController@allwish')->name('wish');
	Route::get('/wish/{id}', 'WishController@wish')->name('wish.add');
	Route::get('/wish/remove/{id}', 'WishController@wishremove')->name('wish.remove');

	Route::get('/add-to-cart/', 'CartController@allcarts')->name('allcarts');
	Route::post('/add-to-cart/{id}', 'CartController@addtocart')->name('cart.add');
	Route::patch('/update-cart', 'CartController@update')->name('cart.update');
	Route::delete('/add-to-cart/remove/', 'CartController@remove')->name('cart.remove');

	Route::post('/checkout/', 'OrderController@checkout')->name('checkout');
	Route::get('/confirm/products/order={id}', 'OrderController@orderproduct');

	Route::get('/{user}/history', 'UserController@history');

	Route::post('/review/store', 'ReviewController@store');
});

Auth::routes();
