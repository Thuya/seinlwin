@extends('layouts.userlayout')
    @section('content')
    <!-- Slider Wrapper -->
    <div class="css-slider-wrapper">
        <input type="radio" name="slider" class="slide-radio1" checked id="slider_1">
        <input type="radio" name="slider" class="slide-radio2" id="slider_2">
        <input type="radio" name="slider" class="slide-radio3" id="slider_3">
        <input type="radio" name="slider" class="slide-radio4" id="slider_4">

        <div class="slider-pagination">
            <label for="slider_1" class="page1"></label>
            <label for="slider_2" class="page2"></label>
            <label for="slider_3" class="page3"></label>
            <label for="slider_4" class="page4"></label>
        </div>
     
        <!-- Slider #1 -->
        <div class="slider slide-1">
            <img src="{{ url('images/model-1.png') }}" alt="">
            <div class="slider-content">
                <h4>New Product</h4>
                <h2>Denim Longline T-Shirt Dress With Split</h2>
                <button type="button" class="buy-now-btn" name="button">$130</button>
            </div>
            <div class="number-pagination">
                <span>1</span>
            </div>
        </div>
     
      <!-- Slider #2 -->
      <div class="slider slide-2">
        <img src="{{ url('images/model-2.png') }}" alt="">
        <div class="slider-content">
          <h4>New Product</h4>
          <h2>Denim Longline T-Shirt Dress With Split</h2>
          <button type="button" class="buy-now-btn" name="button">$130</button>
        </div>
        <div class="number-pagination">
          <span>2</span>
        </div>
      </div>
     
      <!-- Slider #3 -->
      <div class="slider slide-3">
        <img src="{{ url('images/model-3.png') }}" alt="">
        <div class="slider-content">
          <h4>New Product</h4>
          <h2>Denim Longline T-Shirt Dress With Split</h2>
          <button type="button" class="buy-now-btn" name="button">$130</button>
        </div>
        <div class="number-pagination">
          <span>3</span>
        </div>
      </div>
     
      <!-- Slider #4 -->
      <div class="slider slide-4">
        <img src="{{ url('images/model-4.png') }}" alt="">
        <div class="slider-content">
          <h4>New Product</h4>
          <h2>Denim Longline T-Shirt Dress With Split</h2>
          <button type="button" class="buy-now-btn" name="button">$130</button>
        </div>
        <div class="number-pagination">
          <span>4</span>
        </div>
      </div>
    </div>

    <div class="container-fluid mt-5 mb-5">
        <div class="row">
            <div class="col-md-6">
                <div class="men-image img-fluid position-relative rounded">
                    <div class="img-text text-white">
                        <h3 class="text-center" style="font-family: 'Pacifico', cursive;">Men</h3>
                        <a href="{{ route('products.gender', 'Male') }}" class="btn btn-danger rounded-pill">Shop Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="women-image img-fluid position-relative rounded">
                    <div class="img-text">
                        <h3 class="text-center font-weight-bold" style="font-family: 'Pacifico', cursive;">Women</h3>
                        <a href="{{ route('products.gender', 'Female') }}" class="btn btn-danger rounded-pill">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <h1 class="text-center border-bottom pb-3 mb-3" style="font-family: 'Pacifico', cursive;">Blogs</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="https://i.pinimg.com/originals/25/4f/b2/254fb28ffe119ebf93cbebee429044b4.jpg" style="height: 500px;" class="img-fluid w-100 shadow" alt="">
                <div class="text-center">
                    <span style="font-size: 12px;">STYLED</span>
                    <h3 class="mt-2 mb-2" style="font-family: 'Pacifico', cursive;">New Men Fashion</h3>
                    <button class="btn btn-outline-danger rounded-pill">Read More</button>
                </div>
            </div>
            <div class="col-md-4">
                <img src="https://blog.stylewe.com/wp-content/uploads/2016/11/sexy-fashion-style-1.jpg" style="height: 500px;" class="img-fluid w-100 shadow" alt="">
                <div class="text-center">
                    <span style="font-size: 12px;">STYLED</span>
                    <h3 class="mt-2 mb-2" style="font-family: 'Pacifico', cursive;">New Women Fashion</h3>
                    <button class="btn btn-outline-danger rounded-pill">Read More</button>
                </div>
            </div>
            <div class="col-md-4">
                <img src="https://i.pinimg.com/originals/6e/1e/a2/6e1ea2009d9e611c5ed4c6e7ece52b31.jpg" style="height: 500px;" class="img-fluid w-100 shadow" alt="">
                <div class="text-center">
                    <span style="font-size: 12px;">STYLED</span>
                    <h3 class="mt-2 mb-2" style="font-family: 'Pacifico', cursive;">New Couple Fashion</h3>
                    <button class="btn btn-outline-danger rounded-pill">Read More</button>
                </div>
            </div>
        </div>
    </div>

    <footer class="bg-danger pt-3 pb-3 mt-5">
        <p class="text-center m-0 text-white" style="font-size: 18px;">Powered By Seinlwin DT HypeBeast Fashion © 2019</p>
    </footer>
    @endsection