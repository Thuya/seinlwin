@extends('layouts.userlayout')
    @section('content')
        <main class="py-4">
            @yield('content')
        </main>
    @endsection
