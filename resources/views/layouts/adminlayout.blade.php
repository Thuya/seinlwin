<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>DT HypeBeast</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://www.queness.com/resources/html/jcarousel/css/jquery.jcarousel.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <div class="wrapper" id="app">
        <div class="sidebar" data-color="black">
            <!--
            Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

            Tip 2: you can also add an image using data-image tag
            -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="{{ url('/') }}" class="simple-text text-decoration-none">
                        DT HypeBeast
                    </a>
                </div>
                <ul class="nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('admin.products') }}">
                            <i class="nc-icon nc-chart-pie-35"></i>
                            <p>Product</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('admin.categories') }}">
                            <i class="nc-icon nc-backpack"></i>
                            <p>Category</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('admin.brands') }}">
                            <i class="nc-icon nc-apple"></i>
                            <p>Brand</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('admin.colors') }}">
                            <i class="nc-icon nc-chart"></i>
                            <p>Color</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('deliveries') }}">
                            <i class="nc-icon nc-delivery-fast"></i>
                            <p>Deleivery Status</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('orders.all') }}">
                            <i class="nc-icon nc-delivery-fast"></i>
                            <p>Order Status</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('reports') }}">
                            <i class="nc-icon nc-notes"></i>
                            <p>Report status</p>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="{{ route('admin.help') }}">
                            <i class="nc-icon nc-notes"></i>
                            <p>Customer's Message</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <a class="navbar-brand" href="#pablo"> Dashboard </a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <form action="{{ route('admin.search') }}" method="POST">
                                        @csrf
                                        <i class="nc-icon nc-zoom-split ml-2" style="margin-top: 2px;"></i>
                                        <input type="search" name="search" placeholder="Search..." class="border-0">
                                    </form>
                                </a>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('admin.account') }}">
                                    <span class="nc-icon nc-circle-09 mr-1"></span>Account
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <span class="nc-icon nc-button-power mr-1"></span>LogOut
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="content">
                @yield('content')
            </div>
            <footer class="footer">
                <div class="container">
                    <nav>
                        <ul class="footer-menu">
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                        <p class="copyright text-center">
                            ©
                            <a href="#">SeinLwin</a>, made for DT Hybeast fashion
                        </p>
                    </nav>
                </div>
            </footer>
        </div>
    </div>
</body>


<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.js-example-basic-multiple').select2();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();
        $('.fixed-plugin a').click(function(event) {
            // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
            if ($(this).hasClass('switch-trigger')) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else if (window.event) {
                    window.event.cancelBubble = true;
                }
            }
        });

        $('.fixed-plugin .background-color span').click(function() {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            var new_color = $(this).data('color');

            if ($sidebar.length != 0) {
                $sidebar.attr('data-color', new_color);
            }

            if ($full_page.length != 0) {
                $full_page.attr('filter-color', new_color);
            }

            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.attr('data-color', new_color);
            }
        });
    });
</script>
@yield('scripts')
</html>