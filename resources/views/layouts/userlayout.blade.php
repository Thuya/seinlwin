<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>DT Hypebease Fashion</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('styles')
    <style>
        .sticky {
            position: fixed !important;
            top: -3px;
            box-shadow: 0px 1px 12px 0px #000;
            z-index: 15000;
            background-color: #fff !important;
        }
        .sticky a {
            color: #939393;
        }
        .sticky h1 {
            display: block !important;
            color: #939393 !important;
        }
        .sticky img {
            display: none;
        }
        .sticky .navigation-right a {
            color: #939393 !important;
        }
    </style>
</head>
<body>
    <!-- Navigation -->
    <div class="navigation navbar bg-transparent border-0 menu-bar">
        <div class="navigation-left">
            <a href="{{ route('products.type', 'shoes') }}">Shoes</a>
            <a href="{{ route('products.type', 'shirt') }}">Clothing</a>
            <a href="{{ route('journal') }}">Journal</a>
            <a href="{{ route('help') }}">Help</a>
        </div> 
        <div class="navigation-center">
            <h1 class="text-danger text-bold" style="font-family: 'Pacifico', cursive;">DT hypebeast</h1>
            <!-- <img src="{{ url('images/logo.png') }}" alt="logo" class="mt-3"> -->
        </div>
        <div class="navigation-right">
            <a href="{{ route('allcarts') }}" class="mr-4 text-white" style="font-size: 20px;">
                <i class="fas fa-shopping-bag"></i> 
                @if(session('cart'))
                ({{ count(session('cart')) }})
                @else
                0
                @endif
            </a>
            <div class="dropdown">
                <a class="text-white dropdown-toggle mr-3" id="searchDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px; cursor: pointer;">
                <i class="fas fa-search"></i>
                </a>
                <div class="dropdown-menu pl-3 pr-3" style="width: 252px; top: 40px;" aria-labelledby="searchDropdown">
                    <form action="{{ route('products.search') }}" method="POST" class="d-flex">
                        {{ csrf_field() }}
                        <input type="search" name="search" class="dropdown-item form-control border border-right-0">
                        <i class="fas fa-search text-danger p-2 border border-left-0" style="    margin-left: -4px;padding-top: 11px !important;"></i>
                    </form>
                </div>
            </div>
            @guest
            <a class="btn btn-danger rounded-pill" href="{{ route('login') }}">Login acc</a>
            @else
            <div class="dropdown">
                <button class="btn btn-danger rounded-pill dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @if( Auth::user()->role_id == 1 )
                        <a class="dropdown-item" href="{{ route('admin.products') }}">Admin Dashboard</a>
                    @endif
                    <a class="dropdown-item" href="{{ url("/" . Auth::user()->name . "/history" )}}">History</a>
                    <a class="dropdown-item" href="{{ url('/wish/') }}">
                        @if(session('wish'))
                            WishList ({{ count(session('wish')) }})
                        @else
                            WishList (0)
                        @endif
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
            @endguest
        </div>
    </div>

    @yield('content')
     

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(document).on('scroll', function () {
            var position = $(document).scrollTop();
            if (position > 570) {
                $('.menu-bar').addClass('sticky');
            }else {
                $('.menu-bar').removeClass('sticky');
            }
        });
        $('.productFilter').change(function() {
            $('.filterForm').submit();
        });
    </script>
    @yield('scripts')
</body>
</html>
