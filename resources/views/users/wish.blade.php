@extends('layouts.userlayout')
	@section('content')
	    <style>
	    	.navigation {
	    		position: relative;
				box-shadow: 0px 1px 12px 0px #000;
	            z-index: 5;
	            background-color: #fff !important;
	    	}
	    	#card-footer {
			    padding: 0.75rem 1.25rem;
			    background-color: rgba(0, 0, 0, 0.03) !important;
			}
	    	.navigation a, .navigation h1, .navigation .navigation-right a {
	    		color: #939393 !important;
	    	}
	    	.navigation h1 {
	    		display: block !important;
	    	}
	    	.navigation img {
	            display: none;
	        }
	    </style>
	<div class="container mt-5">
		<div class="card">
			<div class="card-body">
				<table id="cart" class="table table-hover">
			        <thead>
				        <tr>
				            <th>Product</th>
				            <th>Price</th>
				            <th>Quantity</th>
				            <th>Action</th>
				        </tr>
			        </thead>
			        <tbody>
			        @if(session('wish'))
			            @foreach(session('wish') as $id => $details)
			                <tr>
			                    <td data-th="Product">
			                        <div class="row">
			                            <div class="col-3 hidden-xs"><img src="{{ $details['photo'] }}" width="100" height="110"/></div>
			                            <div class="col-9">
			                                <h4 class="mb-0 pt-5">{{ $details['name'] }}</h4>
			                            </div>
			                        </div>
			                    </td>
			                    <td data-th="Price">${{ $details['price'] }}</td>
			                    <td data-th="Quantity">
			                        {{ $details['totalqty'] }}
			                    </td>
			                    <td class="actions" data-th="Action" style="padding-top: 53px;">
			                        <a href="{{ url('/wish/remove/', $id) }}" class="btn btn-danger btn-sm remove-from-cart"><i class="fas fa-trash-alt"></i></a>
			                    </td>
			                </tr>
			            @endforeach
			        @endif

			        </tbody>
			    </table>
			    <a href="{{ URL::Previous() }}" class="btn btn-danger rounded-pill"><i class="fa fa-angle-left"></i> Continue Shopping</a>
			</div>
		</div>
	</div>
	@endsection