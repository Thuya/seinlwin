@extends('layouts.userlayout')
    @section('content')

    <style>
    	.navigation {
    		position: relative;
			box-shadow: 0px 1px 12px 0px #000;
            z-index: 5;
            background-color: #fff !important;
    	}
    	#card-footer {
		    padding: 0.75rem 1.25rem;
		    background-color: rgba(0, 0, 0, 0.03) !important;
		}
    	.navigation a, .navigation h1, .navigation .navigation-right a {
    		color: #939393 !important;
    	}
    	.navigation h1 {
    		display: block !important;
    	}
    	.navigation img {
            display: none;
        }
    </style>
	<div class="container mt-5 mb-5">
		@if(session()->has('success'))
			<div class="alert alert-success" role="alert">
				{{ session('success') }}
			</div>
		@endif
		<form action="{{ url('products/filter') }}" method="POST">
			{{ csrf_field() }}
			<div class="form-group">
				<select name="filter" class="form-control in-bg">
					<option selected="selected">-- Filter by --</option>
					<option value="low">Lowest Price</option>
					<option value="high">Hightest Price</option>
				</select>
			</div>
		</form>
		<div class="row">
			@forelse( $products as $product )
				<div class="col-md-3">
					<div class="card">
					  	<img src="{{ url($product->productimages[0]->image) }}" class="card-img-top" alt="" height="300">
					  	<div class="card-body">
					    	<h5 class="card-title">{{ $product->name }}</h5>
					    	<p class="card-text" style="height: 50px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{{ $product->description }}</p>
					  	</div>
					  	<div class="card-footer d-flex" id="card-footer">
					  		<a href="{{ route('product.detail', $product->id) }}" class="btn btn-danger flex-fill rounded-pill pl-4 pr-4">Detail</a>
					  		<?php $wishlist = session()->get('wish'); ?>
					  		@if( isset($wishlist[$product->id]) )
					  		<a href="{{ url('/wish/remove/' . $product->id) }}" class="text-danger flex-fill text-right"><i class="fas fa-heart" style="font-size: 35px;"></i></a>
					  		@else
				      	  	<a href="{{ url('/wish/' . $product->id) }}" class="text-danger flex-fill text-right"><i class="far fa-heart" style="font-size: 35px;"></i></a>
				      	  	@endif
					  	</div>
					</div>
				</div>
			@empty
			<h1>No Product Found!</h1>
			@endforelse
		</div>
	</div>
    @endsection