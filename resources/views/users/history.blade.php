@extends('layouts.userlayout')
	@section('content')
	    <style>
	    	.navigation {
	    		position: relative;
				box-shadow: 0px 1px 12px 0px #000;
	            z-index: 5;
	            background-color: #fff !important;
	    	}
	    	#card-footer {
			    padding: 0.75rem 1.25rem;
			    background-color: rgba(0, 0, 0, 0.03) !important;
			}
	    	.navigation a, .navigation h1, .navigation .navigation-right a {
	    		color: #939393 !important;
	    	}
	    	.navigation h1 {
	    		display: block !important;
	    	}
	    	.navigation img {
	            display: none;
	        }
	        .product-detail {
	            color: #5b5555;
	        }
	    </style>
		<div class="container mt-5">
			<div class="card">
				<div class="card-header"><h3>My History</h3></div>
				<div class="card-body">
					<table class="table table-striped mt-3">
					  	<thead>
						    <tr>
						      <th scope="col">No</th>
						      <th scope="col">Names</th>
						      <th scope="col">Color</th>
						      <th scope="col">Order Date</th>
						      <th scope="col">Payment</th>
						      <th scope="col">Total Quantity</th>
						      <th scope="col">Total Amount</th>
						    </tr>
					  	</thead>
					  	<tbody>
					  		@forelse($orders as $order)
						    <tr>
						      <td>{{ $order->id }}</td>
						      <td>
						      	@foreach($order->orderdetails as $name)
						      		{{ $name->product_name }}
						      		@if(! $loop->last )
						      		,
						      		@endif
						      	@endforeach
						      </td>
						      <td>
						      	@foreach($order->orderdetails as $name)
						      		{{ $name->color }}
						      		@if(! $loop->last )
						      		,
						      		@endif
						      	@endforeach
						      </td>
						      <td>{{ $order->order_date }}</td>
						      <td>{{ $order->payment }}</td>
						      <td>{{ $order->total_quantity }}</td>
						      <td>$ {{ $order->total_amount }}</td>
						    </tr>
						    @empty
								<tr class="text-center">
									<td colspan="4">Not history yet!</td>
								</tr>
							@endforelse
					  	</tbody>
					</table>
					
				</div>
			</div>
		</div>
	@endsection