@extends('layouts.userlayout')
	@section('content')
    <style>
    	.navigation {
    		position: relative;
			box-shadow: 0px 1px 12px 0px #000;
            z-index: 5;
            background-color: #fff !important;
    	}
    	#card-footer {
		    padding: 0.75rem 1.25rem;
		    background-color: rgba(0, 0, 0, 0.03) !important;
		}
    	.navigation a, .navigation h1, .navigation .navigation-right a {
    		color: #939393 !important;
    	}
    	.navigation h1 {
    		display: block !important;
    	}
    	.navigation img {
            display: none;
        }
        .product-detail {
            color: #5b5555;
        }
    </style>
	<div class="container">
		<div class="card pt-4 pb-4">
			<div class="card-body m-auto">
				<h3>Thank You for buying products {{ $buy->user->name }}</h3>
				<p>
					You bought
					 @forelse ( $buys as $buyss )
						{{ $buyss->product_name }}
						@if(! $loop->last )
						,
						@endif
					@empty
						No Product!
					@endforelse
				</p>
				<p class="mb-4">Total Amount is $ {{ $buy->total_amount }}.</p>
				<strong>Confirmation email will be send to your gmail account.</strong><br>
				<strong>Your Product will be deliver soon!</strong><br>
				<a href="/" class="mt-4 btn btn-danger rounded-pill">< Go Back Home</a>
			</div>
		</div>
	</div>
	
	@endsection