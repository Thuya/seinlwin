@extends('layouts/userlayout')
	@section('content')
    <style>
    	.navigation {
    		position: relative;
			box-shadow: 0px 1px 12px 0px #000;
            z-index: 5;
            background-color: #fff !important;
    	}
    	#card-footer {
		    padding: 0.75rem 1.25rem;
		    background-color: rgba(0, 0, 0, 0.03) !important;
		}
    	.navigation a, .navigation h1, .navigation .navigation-right a {
    		color: #939393 !important;
    	}
    	.navigation h1 {
    		display: block !important;
    	}
    	.navigation img {
            display: none;
        }
    </style>
	<div class="container mt-5">
        <h1 class="border-bottom mb-4">Latest 5 Products</h1>
		@forelse( $products as $product )
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <img src="{{ $product->productimages[0]->image }}" class="img-fluid pr-5 border-right" alt="">
                        </div>
                        <div class="col-md-7">
                            <h1>{{ $product->name }}</h1>
                            <div class="d-flex mb-4 mt-3">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star-half-alt"></i>
                                <div class="ml-3" style="margin-top: -6px;">
                                   <a style="text-decoration: underline; cursor: pointer;"> ( 5 Customer review )</a>
                                </div>
                            </div>
                            <strong>$ {{ $product->price }}</strong>
                            <p class="mb-4 mt-5 font-weight-bold">{{ $product->description }} Lorem ipsum dolor sit amet, consectetur adipisicing elit. A nam illo, ducimus eligendi esse, cumque exercitationem. Quaerat adipisci exercitationem inventore maxime, quis nesciunt. Natus nostrum, dolorum culpa provident asperiores optio.</p>
                            <div class="mb-4">Category: <a style="text-decoration: underline; cursor: pointer;">{{ $product->category->name }}</a></div>
                            <a href="{{ route('product.detail', $product->id) }}" class="btn btn-danger rounded-pill pl-4 pr-4">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <h1>No Products!</h1>
        @endforelse
	</div>
	@endsection