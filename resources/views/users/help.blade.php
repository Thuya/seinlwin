@extends('layouts.userlayout')
	@section('content')
	<style>
		.section-feature {
			height: 100%;
		    padding: 20px 20px 15px;
		    background-color: #f5f5f5;
		}
		.help-header {
			padding: 11px 0 7px;
		    text-align: center;
		    background-color: #f5f5f5;
		}
		.section-feature a {
			padding: 6px 0;
		    color: #7d7d7d;
		    line-height: 1.6em;
		    font-size: 15px;
		    display: block;
		}
    	.navigation {
    		position: relative;
			box-shadow: 0px 1px 12px 0px #000;
            z-index: 5;
            background-color: #fff !important;
    	}
    	#card-footer {
		    padding: 0.75rem 1.25rem;
		    background-color: rgba(0, 0, 0, 0.03) !important;
		}
    	.navigation a, .navigation h1, .navigation .navigation-right a {
    		color: #939393 !important;
    	}
    	.navigation h1 {
    		display: block !important;
    	}
    	.navigation img {
            display: none;
        }
        .product-detail {
            color: #5b5555;
        }
    </style>

    <div class="container mt-5">
    	<div class="help-header text-center">
    		<p>Help</p>
    		<h2>Any Questions?</h2>
    		<p>Check out our help or feel free to contact us.</p>
    	</div>
    	<h1 class="text-center mt-5 mb-5">HELP</h1>
    	<div class="row mb-5">
    		<div class="col-sm-6 col-xs-12">
    			<div class="section-feature">
    				<div class="section-header">
    					<h2 class="mb-4 mt-3"><span class="mr-3 rounded-circle bg-white align-middle" style="font-size: 17px; padding: 17px 21px;"><i class="fas fa-boxes"></i></span>YOUR ORDER</h2>
                        <a href="#">Do I need to have an account to place an order?</a>
                        <a href="#">I forgot my password. What should I do?</a>
                        <a href="#">How do I edit or cancel my order?</a>
                        <a href="#">I placed an order but haven't received an order confirmation yet. When will I get it?</a>
                        <a href="#">What’s the status of my order?</a>
    				</div>
    			</div>
    		</div>
    		<div class="col-sm-6 col-xs-12">
    			<div class="section-feature">
    				<div class="section-header">
    					<h2 class="mb-4 mt-3"><span class="mr-3 rounded-circle bg-white align-middle" style="font-size: 17px; padding: 17px 21px;"><i class="fas fa-truck"></i></span>SHIPPING</h2>
                        <a href="#">Where are your items shipped from?</a>
                        <a href="#">What countries do you ship to?</a>
                        <a href="#">What are your shipping costs?</a>
                        <a href="#">My order is eligible for free shipping, but I’m still being charged for shipping. What should I do?</a>
                        <a href="#">Do I need to pay for duty/tax fees?</a>
    				</div>
    			</div>
    		</div>
    	</div>

    	<div class="row mb-5">
    		<div class="col-sm-6 col-xs-12">
    			<div class="section-feature">
    				<div class="section-header">
    					<h2 class="mb-4 mt-3"><span class="mr-3 rounded-circle bg-white align-middle" style="font-size: 17px; padding: 17px 21px;"><i class="fas fa-boxes"></i></span>YOUR ORDER</h2>
                        <a href="#">Do I need to have an account to place an order?</a>
                        <a href="#">I forgot my password. What should I do?</a>
                        <a href="#">How do I edit or cancel my order?</a>
                        <a href="#">I placed an order but haven't received an order confirmation yet. When will I get it?</a>
                        <a href="#">What’s the status of my order?</a>
    				</div>
    			</div>
    		</div>
    		<div class="col-sm-6 col-xs-12">
    			<div class="section-feature">
    				<div class="section-header">
    					<h2 class="mb-4 mt-3"><span class="mr-3 rounded-circle bg-white align-middle" style="font-size: 17px; padding: 17px 21px;"><i class="fas fa-credit-card"></i></span>PAYMENT</h2>
                        <a href="#">How do I change the country or currency settings?</a>
                        <a href="#">Are there exchange rates?</a>
                        <a href="#">How can I pay for my order?</a>
                        <a href="#">Will I be charged any additional fees?</a>
                        <a href="#">My card was charged, but I don't think my order went through. What should I do?</a>
    				</div>
    			</div>
    		</div>
    	</div>

    	<div class="row mb-5 pb-5 border-bottom">
    		<div class="col-sm-6 col-xs-12">
    			<div class="section-feature">
    				<div class="section-header">
    					<h2 class="mb-4 mt-3"><span class="mr-3 rounded-circle bg-white align-middle" style="font-size: 17px; padding: 17px 21px;"><i class="fas fa-tshirt"></i></span>PRODUCT</h2>
                        <a href="#">Are your products authentic?</a>
                        <a href="#">Do you restock any of your products?</a>
                        <a href="#">Do you price match?</a>
                        <a href="#">The price of my product changed during checkout. Why is that?</a>
                        <a href="#">The product I bought is now on promotion. Can I get the difference refunded?</a>
    				</div>
    			</div>
    		</div>
    		<div class="col-sm-6 col-xs-12">
    			<div class="section-feature">
    				<div class="section-header">
    					<h2 class="mb-4 mt-3"><span class="mr-3 rounded-circle bg-white align-middle" style="font-size: 17px; padding: 17px 21px;"><i class="fas fa-undo-alt"></i></span>PRODUCT RETURN</h2>
                        <a href="#">How do I return an item?</a>
                        <a href="#">Do you pay for return shipping costs?</a>
                        <a href="#">How do I know if you received my return?</a>
                        <a href="#">When will I get my refund?</a>
    				</div>
    			</div>
    		</div>
    	</div>

    	<div class="row">
    		<div class="help-footer text-center w-100">
    			<h3>Other Questions?</h3>
			    <p>We offer support in <strong>English</strong>, <strong>Mandarin</strong> and <strong>Cantonese</strong>.</p>
			    <div class="support">
			        <div class="support-btn col-md-12">
			            <button class="btn btn-dark mb-3" data-toggle="modal" data-target="#exampleModal">
			                <span>help@hbx.com</span>
			            </button>
			            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						        <form action="{{ route('help.store') }}" method="POST" class="text-left">
						        	@csrf
						        	<div class="form-group">
						        		<label>Name</label>
						        		<input type="text" name="name" class="form-control" required>
						        	</div>
						        	<div class="form-group">
						        		<label>Email</label>
						        		<input type="email" name="email" class="form-control" required>
						        	</div>
						        	<div class="form-group">
						        		<label>Reason</label>
						        		<textarea name="reason" id="reason" class="form-control" cols="30" rows="10"></textarea>
						        	</div>
						        	<button class="btn btn-success float-right">Send</button>
						        </form>
						      </div>
						    </div>
						  </div>
						</div>
			            <p>Send us an email and we'll get back to you within 24 hours.<br><span>*Please note: Email response times may take longer due to the holiday period.</span></p>
			        </div>
                </div>
    		</div>
    	</div>
    </div>
	@endsection