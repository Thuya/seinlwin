@extends('layouts/userlayout')
    
    @section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/img/controls.png">
    @endsection

	@section('content')
    <style>
        input[type=number]::-webkit-inner-spin-button, 
        input[type=number]::-webkit-outer-spin-button { 
          -webkit-appearance: none; 
          margin: 0; 
        }
         #lightSlider {
             max-height: 343px;
         }
         .lSAction>a {
             background-color: rgba(0,0,0, 0.8);
         }
         #lightSlider li img {
             width: 100%;
             max-height: 343px;
         }
         .lSGallery {
             text-align: center;
             width: 100% !important;
         }
        .navigation {
            position: relative;
            box-shadow: 0px 1px 12px 0px #000;
            z-index: 5;
            background-color: #fff !important;
        }
        #card-footer {
            padding: 0.75rem 1.25rem;
            background-color: rgba(0, 0, 0, 0.03) !important;
        }
        .navigation a, .navigation h1, .navigation .navigation-right a {
            color: #939393 !important;
        }
        .navigation h1 {
            display: block !important;
        }
        .navigation img {
            display: none;
        }
    </style>
    @if(session()->has('success'))
        <div class="alert alert-success position-absolute" style="top: 10px;right: 10px; z-index: 1000;" role="alert">
            {{ session('success') }}
        </div>
    @endif
	<div class="container mt-5">
		<div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-body">
                                    <ul id="lightSlider">
                                        @foreach($product->productimages as $image)
                                        <li data-thumb="{{ url($image->image) }}">
                                            <img src="{{ url($image->image) }}" />
                                        </li>
                                        @endforeach
                                        @foreach($product->productimages as $image)
                                        <li data-thumb="{{ url($image->image) }}">
                                            <img src="{{ url($image->image) }}" />
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>         
                        </div>
                        <div class="col-md-7 product-detail">
                            <h1>{{ $product->name }}</h1>
                            <div class="d-flex mb-4 mt-3">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star-half-alt"></i>
                                <div class="ml-3" style="margin-top: -6px;">
                                   <a style="text-decoration: underline; cursor: pointer;"> ( {{ count($product->reviews) }} Customer review )</a>
                                </div>
                            </div>
                            <strong>$ {{ $product->price }}</strong>
                            <p class="mb-4 mt-5 font-weight-bold">{{ $product->description }} Lorem ipsum dolor sit amet, consectetur adipisicing elit. A nam illo, ducimus eligendi esse, cumque exercitationem. Quaerat adipisci exercitationem inventore maxime, quis nesciunt. Natus nostrum, dolorum culpa provident asperiores optio.</p>
                            <div class="mb-4">Category: <a style="text-decoration: underline; cursor: pointer;">{{ $product->category->name }}</a></div>
                            <div class="d-flex">
                                <h4>Share to : </h4>
                                <a class="ml-3" style="cursor: pointer; font-size: 25px; color: #1b1bba; line-height: 29px;" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}&display=popup"><i class="fab fa-facebook"></i></a>
                                <a class="ml-2" style="cursor: pointer; font-size: 25px; color: #06b5eb; line-height: 29px;" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}&display=popup"><i class="fab fa-twitter"></i></a>
                                <a class="ml-2" style="cursor: pointer; font-size: 25px; color: red;line-height: 29px;" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}&display=popup"><i class="fab fa-instagram"></i></a>
                                <a class="ml-2" style="cursor: pointer; font-size: 25px; color: #e7556e;line-height: 29px;" href="https://www.facebook.com/sharer/sharer.php?u={{ URL::current() }}&display=popup"><i class="fab fa-dribbble"></i></a>
                            </div>
                            <hr>
                            
                            <form action="{{ route('cart.add', $product->id) }}" method="POST" class=" w-100">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Size :</label>
                                    <select name="size" class="form-control">
                                        <option value="S">S</option>
                                        <option value="M">M</option>
                                        <option value="L">L</option>
                                        <option value="XL">XL</option>
                                    </select>
                                </div>
                                <?php $carts = session()->get('cart'); ?>
                                @if(! isset($carts[$product->id]))
                                <div class="form-group">
                                    <label>Color :</label>
                                    <select name="color" class="form-control">
                                        @foreach( $product->colorproducts as $color )
                                            <option value="{{ $color->color->name }}">{{ $color->color->name }}</option>
                                        @endforeach
                                    </select> 
                                </div>
                                @endif        
                                <div class="row mt-4 mb-5">  
                                    @if(! isset($carts[$product->id]) )
                                    <div class="col-md-6 d-flex quantity">
                                        <input type="number" name="quantity" value="1" min="1" max="{{ $product->totalqty }}" class="form-control" style="width: 50px;" required> <h4 style="line-height: 33px;" class="ml-2">: Add Quantity</h4>
                                    </div>
                                    @else
                                    <p class="col-md-6">Product Already add and please check your <a href="{{ route('allcarts') }}">cart</a>!</p>
                                    @endif
                        
                                    @if( isset($carts[$product->id]) )
                                    <div class="col-md-6">
                                        <a title="This product is already added to cart" class="btn btn-danger rounded-pill float-right" disabled>Add to Cart</a>
                                    </div>
                                    @else
                                    <div class="col-md-6">
                                        <button class="btn btn-danger rounded-pill float-right">Add to Cart</button>
                                    </div>
                                    @endif
                                </div>
                            </form>
                            <h2>Write Your Review</h2>
                            <hr>
                            <form action="{{ url('/review/store') }}" method="POST" class="mt-3 mb-3 review">
                                @csrf
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <textarea name="review" class="form-control" cols="30" rows="5"></textarea>
                                <button class="btn btn-danger rounded-pill mt-3">Submit</button>
                            </form>
                            <h3 class="mt-5">People Review</h3>
                            <hr>
                            <ul class="list-unstyled mt-5 mb-5">
                                @forelse( $product->reviews as $review )
                                <li class="media mb-4">
                                    <div class="rounded-circle pt-2 pb-2 pl-3 pr-3 bg-light mr-3 border">
                                        <i class="fas fa-user"></i>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-1">{{ $review->user->name }} <span style="font-size: 12px; margin-left: 20px;">{{ Carbon\Carbon::parse($review->created_at)->diffForHumans() }}</span></h5> 
                                        {{ $review->review }}
                                    </div>
                                </li>
                                @empty
                                <h4>No reviews!</h4>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
	</div>
	@endsection

    @section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
       $('#lightSlider').lightSlider({
           gallery: true,
           item: 1,
           loop: true,
           slideMargin: 0,
           thumbItem: 9
       });

       // $(".review").submit(function (e) {
       //     e.preventDefault();

       //     var ele = $(this);

       //      $.ajax({
       //         url: '{{ url('update-cart') }}',
       //         method: "patch",
       //         data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
       //         success: function (response) {
       //             window.location.reload();
       //         }
       //      });
       //  });
    });
    </script>
    @endsection