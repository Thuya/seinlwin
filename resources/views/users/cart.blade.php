@extends('layouts/userlayout')
	@section('content')
	    <style>
	    	.navigation {
	    		position: relative;
				box-shadow: 0px 1px 12px 0px #000;
	            z-index: 5;
	            background-color: #fff !important;
	    	}
	    	#card-footer {
			    padding: 0.75rem 1.25rem;
			    background-color: rgba(0, 0, 0, 0.03) !important;
			}
	    	.navigation a, .navigation h1, .navigation .navigation-right a {
	    		color: #939393 !important;
	    	}
	    	.navigation h1 {
	    		display: block !important;
	    	}
	    	.navigation img {
	            display: none;
	        }
	        .product-detail {
	            color: #5b5555;
	        }
	    </style>
		<div class="container-fluid mt-5">
			@if(session()->has('success'))
				<div class="alert alert-success mt-4" role="alert">
					{{ session('success') }}
				</div>
			@endif
			<div class="row">
				<div class="col-md-7">
					<div class="card">
						<div class="card-body">
							<table id="cart" class="table table-hover table-condensed table-responsive">
						        <thead>
						        <tr>
						            <th style="width:45%">Product</th>
									<th style="width: 5%;">Color</th>
						            <th style="width:10%">Price</th>
						            <th style="width:8%">Quantity</th>
						            <th style="width:22%" class="text-center">Subtotal</th>
						            <th style="width:10%">Actions</th>
						        </tr>
						        </thead>
						        <tbody>

						        <?php $total = 0 ?>

						        @if(session('cart'))
						            @foreach(session('cart') as $id => $details)

						                <?php $total += $details['price'] * $details['quantity'] ?>

						                <tr class="border-bottom">
						                    <td data-th="Product">
						                        <div class="row">
						                            <div class="col-12 hidden-xs">
						                            	<img src="{{ $details['photo'] }}" width="100" height="130"/>
						                            	<p class="mb-0">{{ $details['name'] }}</p>
						                            </div>        
						                        </div>
						                    </td>
						                    <td data-th="Color" class="position-relative">
						                    	<span class="position-absolute" style="background-color: {{ $details['color'] }}; width: 20px; height: 20px; top: 78px;"></span>
						                    </td>
						                    <td data-th="Price">${{ $details['price'] }}</td>
						                    <td data-th="Quantity">
						                        <input type="number" min="1" max="{{ $details['totalqty'] }}" value="{{ $details['quantity'] }}" class="form-control quantity" />
						                    </td>
						                    <td data-th="Subtotal" class="text-center">${{ $details['price'] * $details['quantity'] }}</td>
						                    <td class="actions" data-th="Actions" style="padding-top: 74px;">
						                        <button class="btn btn-info btn-sm update-cart text-white mr-2" data-id="{{ $id }}"><i class="fas fa-wrench"></i></button>
						                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fas fa-times"></i></button>
						                    </td>
						                </tr>
						            @endforeach
						        @endif

						        </tbody>
						        <tfoot>
						        <tr>
						            <td><a href="{{ URL::previous() }}" class="btn btn-danger rounded-pill"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
						            <td colspan="3" class="hidden-xs"></td>
						            <td class="hidden-xs text-center">
						            	<strong>Total : ${{ $total }}</strong>
						            </td>
						        </tr>
						        </tfoot>
						    </table>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="card">
						<div class="card-header">Check Out Form</div>
						<form action="{{ route('checkout') }}" method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
							<div class="card-body">
								<div class="form-group row">
								    <label for="colFormLabel" class="col-sm-3 col-form-label">Contact Person</label>
								    <div class="col-sm-9">
								      	<input type="text" name="contact_person" value="{{ Auth::user()->name }}" class="form-control" required>
								    </div>
								</div>
								<div class="form-group row">
								    <label for="colFormLabel" class="col-sm-3 col-form-label">Payment Method</label>
								    <div class="col-sm-9">
								      	<select name="payment" class="form-control">
								      		<option value="Visa">Visa</option>
								      		<option value="Master">Master</option>
								      		<option value="Credit">Credit</option>
								      		<option value="MPU">MPU</option>
								      	</select>
								    </div>
								</div>
								<div class="form-group row">
								    <label for="colFormLabel" class="col-sm-3 col-form-label">Total Amount($)</label>
								    <div class="col-sm-9">
								      	<input name="total_amount" class="form-control" value="{{ $total }}" readonly>
								    </div>
								</div>
								<div class="form-group row">
								    <label for="colFormLabel" class="col-sm-3 col-form-label">Total Product</label>
								    <div class="col-sm-9">
								      	<input type="number" name="total_quantity" class="form-control" value="{{ count(session('cart')) }}" readonly>
								    </div>
								</div>
								<div class="form-group row">
								    <label for="colFormLabel" class="col-sm-3 col-form-label">Order Date</label>
								    <div class="col-sm-9">
								      	<input name="order_date" value="{{ date('Y-m-d') }}" class="form-control" readonly>
								    </div>
								</div>
								<input name="order_month" value="{{ $month }}" hidden>
								<div class="form-group row">
								    <label for="colFormLabel" class="col-sm-3 col-form-label">Phone No</label>
								    <div class="col-sm-9">
								      	<input type="phone" class="form-control" required>
								    </div>
								</div>
								<div class="form-group row">
								    <label for="colFormLabel" class="col-sm-3 col-form-label">Address</label>
								    <div class="col-sm-9">
								      	<textarea name="address"  class="form-control" required></textarea>
								    </div>
								</div>
							</div>
							<div class="card-footer">
								<button class="btn btn-danger rounded-pill">Make Payment</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('scripts')	
	<script type="text/javascript">

	    $(".update-cart").click(function (e) {
	       e.preventDefault();

	       var ele = $(this);

	        $.ajax({
	           url: '{{ url('update-cart') }}',
	           method: "patch",
	           data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
	           success: function (response) {
	               window.location.reload();
	           }
	        });
	    });

	    $(".remove-from-cart").click(function (e) {
	        e.preventDefault();

	        var ele = $(this);

	        if(confirm("Are you sure")) {
	            $.ajax({
	                url: '{{ url('/add-to-cart/remove/') }}',
	                method: "DELETE",
	                data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
	                success: function (response) {
	                    window.location.reload();
	                }
	            });
	        }
	    });

	    $(".paid").click(function (e) {
	    	e.preventDefault();
	    	$(".checkoutForm").submit();
	    });
	</script>
	@endsection