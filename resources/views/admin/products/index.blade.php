@extends('layouts/adminlayout')
	@section('content')
	<div class="container-fluid">
		@if(session()->has('message'))
			<div class="alert alert-{{ session('status') }} mt-2" role="alert">
				{{ session('message') }}
			</div>
		@endif
		<a href="{{ route('products.create') }}" class="btn btn-success">Create</a>
		<table class="table table-striped mt-3">
		  	<thead>
		    	<tr>
		      		<th scope="col">No</th>
		      		<th scope="col">Name</th>
		      		<th scope="col">Color</th>
		      		<th scope="col">Category</th>
		      		<th scope="col">Brand</th>
		      		<th scope="col">Actions</th>
		    	</tr>
		  	</thead>
		  	<tbody>
		   	@forelse( $products as $product )
		   		<tr>
		   			<th scope="row">{{ $product->id }}</th>
		   			<td>{{ $product->name }}</td>
		   			
		   			<td class="position-relative">
		   				<ul class="list-inline-item pl-0">
		   					@foreach($product->colorproducts as $color)
		   					<li class="list-inline-item border" style="background-color: {{ $color->color->name  }}; width: 20px; height: 20px;"></li>
		   					@endforeach
		   				</ul>
		   			</td>

		   			<td>{{ $product->category->name }}</td>
		   			<td>{{ $product->brand->name }}</td>
		   			<td>
		   				<a href="{{ route('products.show', $product->id) }}" title="detail of {{ $product->name }}" class="btn btn-info text-white text-bold"><i class="nc-icon nc-album-2"></i></a>
		   				<a href="{{ route('products.edit', $product->id) }}" title="edit" class="btn btn-warning"><i class="nc-icon nc-settings-tool-66"></i></a>
		   				<a href="{{ route('products.delete', $product->id) }}" title="delete" class="btn btn-danger text-white"><i class="nc-icon nc-simple-remove"></i></a>
		   			</td>
		   		</tr>
		   	@empty
		   	<tr>
		   		<td colspan="3"><h3>No Products!</h3></td>
		   	</tr>
		   	@endforelse
		  </tbody>
		</table>
	</div>
	@endsection