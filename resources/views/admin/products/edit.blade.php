@extends('layouts/adminlayout')
	@section('content')
	<div class="container-fluid">
		<div class="card">
			<form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="{{ $product->id }}">
				<div class="card-body">
					<div class="form-group">
						<label>Product Name</label>
						<input type="text" name="name" value="{{ $product->name }}" class="form-control">
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Category</label>
								<select name="category_id" class="form-control js-example-basic-single">
									@foreach( $categories as $category )
										<option value="{{ $category->id }}"
											@if( $category->id == $product->category_id )
												selected = "selected"
											@endif
											>{{ $category->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Brand</label>
								<select name="brand_id" class="form-control js-example-basic-single">
									@foreach( $brands as $brand )
										<option value="{{ $brand->id }}" 
											@if( $brand->id == $product->brand_id )
												selected = "selected"
											@endif
											>{{ $brand->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Color</label>
								
								<select name="color_id[]" class="form-control js-example-basic-multiple" multiple>
									@foreach( $colors as $color => $col)
										<option value="{{ $col->id }}" 
											@foreach( $selected_color as $color )
												@if($col->id == $color)
												selected = "selected"
												@endif
											@endforeach 
											>{{ $col->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Gender</label>
								<select name="gender" class="form-control">
									@if($product->gender == 'Male')
										<option value="Male" selected="selected">Male</option>
										<option value="Female">Female</option>
									@else
										<option value="Male">Male</option>
										<option value="Female" selected="selected">Female</option>
									@endif
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Price</label>
								<input type="number" min="1" value="{{ $product->price }}" name="price" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Code</label>
								<input type="text" name="code" value="{{ $product->code }}" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea name="description" class="form-control" cols="30" rows="5">{{ $product->description }}</textarea>
					</div>

					<!-- <div class="row"> -->
						<!-- <div class="col-md-6">
							<div class="form-group">
								<label class="d-block">Images</label>
								<input type="file" name="images[]" multiple="multiple"  
								value="
									@foreach($product->productimages as $image)
									{{ $image->image }}
									@endforeach
								"
								>
							</div>
						</div> -->
						<!-- <div class="col-md-6"> -->
							<div class="form-group">
								<label>Quantity</label>
								<input type="number" name="totalqty" class="form-control" value="{{ $product->totalqty }}">
							</div>
						<!-- </div> -->
					<!-- </div> -->
				</div>
				<div class="card-footer">
					<button class="btn btn-warning">Update</button>
				</div>
			</form>
		</div>
	</div>
	@endsection