@extends('layouts/adminlayout')
	@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7">
				<div id="welcomeHero">	
					<div id="slideshow-main">
						<ul>
							@foreach($product->productimages as $image)
								<li class="p{{ $image->id }} active" style="height: 392px;">
									<a href="#" class="d-block">
										<img src="{{ url($image->image) }}" width="430" height="392" alt=""/>
										<span class="opacity"></span>
										<span class="content"><h1>{{ $product->name }}</h1></span>
									</a>
								</li>
							@endforeach
							<!-- extra -->
							@foreach($product->productimages as $image)
									<li class="p1{{ $image->id }} active" style="height: 392px;">
										<a href="#" class="d-block">
											<img src="{{ url($image->image) }}" width="430" height="392" alt=""/>
											<span class="opacity"></span>
											<span class="content"><h1>{{ $product->name }}</h1></span>
										</a>
									</li>
							@endforeach
						</ul>										
					</div>
								
					<div id="slideshow-carousel">				
						<ul id="carousel" class="jcarousel jcarousel-skin-tango">
							@foreach($product->productimages as $image)
								<li><a href="#" rel="p{{ $image->id }}"><img src="{{ url($image->image) }}" width="206" height="132" alt="#"/></a></li>
							@endforeach
							<!-- extra -->
							@foreach($product->productimages as $image)
								<li><a href="#" rel="p1{{ $image->id }}"><img src="{{ url($image->image) }}" width="206" height="132" alt="#"/></a></li>
							@endforeach
						</ul>
					</div>
						
					<div class="clear"></div>
				</div>
			</div>
			<div class="col-md-5">
				<ul class="list-group">
				  	<li class="list-group-item active bg-danger">
				  		<h3 class="m-0">{{ $product->name }}</h3>
				  	</li>
				  	<li class="list-group-item">
				  		<p class="m-0">Brand => {{ $product->brand->name }}</p>
				  	</li>
				  	<li class="list-group-item">
				  		<p class="m-0">Category => {{ $product->category->name }}</p>
				  	</li>
				  	<li class="list-group-item">
				  		<div class="d-flex">
				  			<b class="mr-2">Color => </b>
				  			@foreach($product->colorproducts as $color)
				  			<div class="list-inline-item border" style="background-color: {{ $color->color->name  }}; width: 20px; height: 20px;"></div>
				  			@endforeach
				  		</div>
				  	</li>
				  	<li class="list-group-item">
				  		<p class="m-0">Price => {{ $product->price }}</p>
				  	</li>
				  	<li class="list-group-item">
				  		<p class="m-0">Code => {{ $product->code }}</p>
				  	</li>
				  	<li class="list-group-item">
				  		<p class="m-0">Gender => {{ $product->gender }}</p>
				  	</li>
				  	<li class="list-group-item">
				  		<p class="m-0">Total Quantity => {{ $product->totalqty }}</p>
				  	</li>
				</ul>
			</div>
		</div>
	</div>
	@endsection

	@section('scripts')
	<script src="https://www.queness.com/resources/html/jcarousel/js/jquery-1.4.2.min.js"></script>
	<script src="https://www.queness.com/resources/html/jcarousel/js/jquery.jcarousel.pack.js"></script>
	<script>
		$(document).ready(function () {
				
			//jCarousel Plugin
		    $('#carousel').jcarousel({
				vertical: true,
				scroll: 1,
				auto: 2,
				wrap: 'last',
				initCallback: mycarousel_initCallback
		   	});

			//Front page Carousel - Initial Setup
		   	$('div#slideshow-carousel a img').css({'opacity': '0.5'});
		   	$('div#slideshow-carousel a img:first').css({'opacity': '1.0'});
		   	$('div#slideshow-carousel li a:first').append('<span class="arrow"></span>')

		  
		  	//Combine jCarousel with Image Display
		    $('div#slideshow-carousel li a').hover(
		       	function () {
		        		
		       		if (!$(this).has('span').length) {
		        		$('div#slideshow-carousel li a img').stop(true, true).css({'opacity': '0.5'});
		   	    		$(this).stop(true, true).children('img').css({'opacity': '1.0'});
		       		}		
		       	},
		       	function () {
		        		
		       		$('div#slideshow-carousel li a img').stop(true, true).css({'opacity': '0.5'});
		       		$('div#slideshow-carousel li a').each(function () {

		       			if ($(this).has('span').length) $(this).children('img').css({'opacity': '1.0'});

		       		});
		        		
		       	}
			).click(function () {

			      	$('span.arrow').remove();        
				$(this).append('<span class="arrow"></span>');
		       	$('div#slideshow-main li').removeClass('active');        
		       	$('div#slideshow-main li.' + $(this).attr('rel')).addClass('active');	
		        	
		       	return false;
			});
		});


		//Carousel Tweaking

		function mycarousel_initCallback(carousel) {
			
			// Pause autoscrolling if the user moves with the cursor over the clip.
			carousel.clip.hover(function() {
				carousel.stopAuto();
			}, function() {
				carousel.startAuto();
			});
		}
	</script>
	@endsection