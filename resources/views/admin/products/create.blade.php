@extends('layouts/adminlayout')
	@section('content')
	<div class="container-fluid">
		<div class="card">
			<form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="card-body">
					<div class="form-group">
						<label>Product Name</label>
						<input type="text" name="name" class="form-control">
						@if( $errors->has('name') )
							<div class="alert alert-danger mt-2" role="alert">
			   					{{ $errors->first('name') }}
			   				</div>
						@endif
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Category</label>
								<select name="category_id" class="form-control js-example-basic-single">
									@foreach( $categories as $category )
										<option value="{{ $category->id }}">{{ $category->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Brand</label>
								<select name="brand_id" class="form-control js-example-basic-single">
									@foreach( $brands as $brand )
										<option value="{{ $brand->id }}">{{ $brand->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Color</label>
								<select name="color_id[]" class="form-control js-example-basic-multiple" multiple>
									@foreach( $colors as $color )
										<option value="{{ $color->id }}">{{ $color->name }}</option>
									@endforeach
								</select>
								@if( $errors->has('color_id') )
									<div class="alert alert-danger mt-2" role="alert">
					   					{{ $errors->first('color_id') }}
					   				</div>
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Gender</label>
								<select name="gender" class="form-control">
									<option value="Male">Male</option>
									<option value="Female">Female</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Price</label>
								<input type="number" min="1" name="price" class="form-control">
								@if( $errors->has('price') )
									<div class="alert alert-danger mt-2" role="alert">
					   					{{ $errors->first('price') }}
					   				</div>
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Code</label>
								<input type="text" name="code" value="DT-{{ rand(120001, 199999) }}" class="form-control">
								@if( $errors->has('code') )
									<div class="alert alert-danger mt-2" role="alert">
					   					{{ $errors->first('code') }}
					   				</div>
								@endif
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea name="description" class="form-control" cols="30" rows="5"></textarea>
						@if( $errors->has('description') )
							<div class="alert alert-danger mt-2" role="alert">
			   					{{ $errors->first('description') }}
			   				</div>
						@endif
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="d-block">Product Type</label>
								<select name="product_type" class="form-control">
									<option value="shirt">Shirt</option>
									<option value="shoes">Shoe</option>
								</select>
								@if( $errors->has('images') )
									<div class="alert alert-danger mt-2" role="alert">
					   					{{ $errors->first('images') }}
					   				</div>
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Quantity</label>
								<input type="number" name="totalqty" class="form-control">
								@if( $errors->has('totalqty') )
									<div class="alert alert-danger mt-2" role="alert">
					   					{{ $errors->first('totalqty') }}
					   				</div>
								@endif
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="d-block">Images</label>
						<input type="file" name="images[]" multiple="multiple">
						@if( $errors->has('images') )
							<div class="alert alert-danger mt-2" role="alert">
			   					{{ $errors->first('images') }}
			   				</div>
						@endif
					</div>
				</div>
				<div class="card-footer">
					<button class="btn btn-success">Create</button>
				</div>
			</form>
		</div>
	</div>
	@endsection