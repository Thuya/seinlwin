@extends('layouts/adminlayout')
	@section('content')

	<table class="table table-striped mt-3">
	  	<thead>
		    <tr>
		      <th scope="col">No</th>
		      <th scope="col">User Name</th>
		      <th scope="col">Email</th>
		      <th scope="col">Product Name</th>
		      <th scope="col">Total Quantity</th>
		      <th scope="col">Total Amount</th>
		    </tr>
	  	</thead>
	  	<tbody>
	  		@forelse($orders as $order)
		    <tr>
		      <th scope="row">{{ $order->id }}</th>
		      <td>{{ $order->user->name }}</td>
		      <td>{{ $order->user->email }}</td>
		      <td>
		      	@foreach($order->orderdetails as $name)
		      		{{ $name->product_name }}
		      		@if(! $loop->last )
		      		,
		      		@endif
		      	@endforeach
		      </td>
		      <td>{{ $order->total_quantity }}</td>
		      <td>$ {{ $order->total_amount }}</td>
		    </tr>
		    @empty
				<tr class="text-center">
					<td colspan="6">No history yet!</td>
				</tr>
			@endforelse
	  	</tbody>
	</table>
	@endsection