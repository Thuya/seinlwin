@extends('layouts/adminlayout')
	@section('content')

	<h2>Messages Lists</h2>
	<hr>
	<table class="table table-striped mt-3">
	  	<thead>
		    <tr>
		      	<th scope="col">No</th>
		      	<th scope="col">Name</th>
		      	<th scope="col">Email</th>
		      	<th scope="col">Reason</th>
		      	<th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
		    @forelse( $helps as $help )
		    	<tr>
		    		<th scope="row">{{ $help->id }}</th>
		    		<td>{{ $help->name }}</td>
		    		<td>{{ $help->email }}</td>
		    		<td>{{ $help->reason }}</td>
		    		<td>
		    			<a href="{{ route('help.delete', $help->id) }}" class="btn btn-danger rounded-pill">Delete</a>
		    		</td>
		    	</tr>
		    @empty
		    <tr>
		    	<td colspan="3"><h3>No Messages!</h3></td>
		    </tr>
		    @endforelse
	  	</tbody>
	</table>
	@endsection