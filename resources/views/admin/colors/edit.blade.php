@extends('layouts/adminlayout')
	@section('content')
	<div class="card">
		<form action="{{ route('colors.update', $color->id) }}" method="POST">
			{{ csrf_field() }}
			<input type="hidden" name="id" value="{{ $color->id }}">
			<div class="card-body">
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" value="{{ $color->name }}" class="form-control">
						@if ($errors->has('name'))
			   				<div class="alert alert-danger mt-2" role="alert">
			   					{{ $errors->first('name') }}
			   				</div>
						@endif
				</div>
			</div>
			<div class="card-footer">
				<button class="btn btn-warning">Update</button>
			</div>
		</form>
	</div>
	@endsection