@extends('layouts/adminlayout')
	@section('content')
	<div class="container-fluid">
		@if(session()->has('message'))
			<div class="alert alert-{{ session('status') }} mt-2" role="alert">
				{{ session('message') }}
			</div>
		@endif
		<a href="{{ route('colors.create') }}" class="btn btn-success">Create</a>
		<table class="table table-striped mt-3">
		  	<thead>
		    	<tr>
		      		<th scope="col">No</th>
		      		<th scope="col">Color Name</th>
		      		<th scope="col">Actions</th>
		    	</tr>
		  	</thead>
		  	<tbody>
		   	@forelse( $colors as $color )
		   		<tr>
		   			<th scope="row">{{ $color->id }}</th>
		   			<td>{{ $color->name }}</td>
		   			<td>
		   				<a href="{{ route('colors.edit', $color->id) }}" title="edit" class="btn btn-warning"><i class="nc-icon nc-settings-tool-66"></i></a>
		   				<a href="{{ route('colors.delete', $color->id) }}" title="delete" class="btn btn-danger text-white"><i class="nc-icon nc-simple-remove"></i></a>
		   			</td>
		   		</tr>
		   	@empty
		   	<tr>
		   		<td colspan="3"><h3>No Color!</h3></td>
		   	</tr>
		   	@endforelse
		  </tbody>
		</table>
	</div>
	@endsection