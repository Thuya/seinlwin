@extends('layouts/adminlayout')
	@section('content')
	<div class="container-fluid">
		@if(session()->has('message'))
			<div class="alert alert-{{ session('status') }} mt-2" role="alert">
				{{ session('message') }}
			</div>
		@endif
		<a href="{{ route('brands.create') }}" class="btn btn-success">Create</a>
		<table class="table table-striped mt-3">
		  	<thead>
		    	<tr>
		      		<th scope="col">No</th>
		      		<th scope="col">Brand Name</th>
		      		<th scope="col">Actions</th>
		    	</tr>
		  	</thead>
		  	<tbody>
		   	@forelse( $brands as $brand )
		   		<tr>
		   			<th scope="row">{{ $brand->id }}</th>
		   			<td>{{ $brand->name }}</td>
		   			<td>
		   				<a href="{{ route('brands.edit', $brand->id) }}" title="edit" class="btn btn-warning"><i class="nc-icon nc-settings-tool-66"></i></a>
		   				<a href="{{ route('brands.delete', $brand->id) }}" title="delete" class="btn btn-danger text-white"><i class="nc-icon nc-simple-remove"></i></a>
		   			</td>
		   		</tr>
		   	@empty
		   	<tr>
		   		<td colspan="3"><h3>No Brand!</h3></td>
		   	</tr>
		   	@endforelse
		  </tbody>
		</table>
	</div>
	@endsection