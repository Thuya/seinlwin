@extends('layouts/adminlayout')
	@section('content')
	@if(session()->has('success'))
		<div class="alert alert-success mt-2" role="alert">
			{{ session('success') }}
		</div>
	@endif
	<h2>Generate Report</h2>
	<hr>
	<a href="{{ route('reports.all') }}" class="btn btn-danger">Sale All</a>
	<a href="{{ route('reports.monthly') }}" class="btn btn-danger">Monthly Sale</a>
	<a href="{{ route('reports.daily') }}" class="btn btn-danger">Today Sale</a>
	<h2 class="mt-4">Stock Control And Purchase</h2>
	<hr>
	<table class="table table-striped mt-3">
	  	<thead>
		    <tr>
		      	<th scope="col">No</th>
		      	<th scope="col">Name</th>
		      	<th scope="col">Color</th>
		      	<th scope="col">Category</th>
		      	<th scope="col">Brand</th>
		      	<th scope="col">Quantity</th>
		      	<th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
		    @forelse( $reports as $report )
		    	<tr>
		    		<th scope="row">{{ $report->id }}</th>
		    		<td>{{ $report->name }}</td>
		    		<td class="position-relative">
		    			<ul class="list-inline-item pl-0">
		   					@foreach($report->colorproducts as $color)
		   					<li class="list-inline-item border" style="background-color: {{ $color->color->name  }}; width: 20px; height: 20px;"></li>
		   					@endforeach
		   				</ul>
		    		</td>
		    		<td>{{ $report->category->name }}</td>
		    		<td>{{ $report->brand->name }}</td>
		    		<td>{{ $report->totalqty }}</td>
		    		<td>
		    			<a href="{{ route('purchases', $report->id) }}" title="purchase product from distributors" class="btn btn-danger rounded-pill">Purchase</a>
		    		</td>
		    	</tr>
		    @empty
		    <tr>
		    	<td colspan="3"><h3>No Products!</h3></td>
		    </tr>
		    @endforelse
	  	</tbody>
	</table>
	@endsection