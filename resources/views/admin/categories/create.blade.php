@extends('layouts/adminlayout')
	@section('content')
	<div class="card">
		<form action="{{ route('categories.store') }}" method="POST">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group">
					<label>Name</label>
					<input type="text" name="name" class="form-control">
						@if ($errors->has('name'))
			   				<div class="alert alert-danger mt-2" role="alert">
			   					{{ $errors->first('name') }}
			   				</div>
						@endif
				</div>
			</div>
			<div class="card-footer">
				<button class="btn btn-success">Create</button>
			</div>
		</form>
	</div>
	@endsection