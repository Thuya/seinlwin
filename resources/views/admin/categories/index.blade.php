@extends('layouts/adminlayout')
	@section('content')
	<div class="container-fluid">
		@if(session()->has('message'))
			<div class="alert alert-{{ session('status') }} mt-2" role="alert">
				{{ session('message') }}
			</div>
		@endif
		<a href="{{ route('categories.create') }}" class="btn btn-success">Create</a>
		<table class="table table-striped mt-3">
		  	<thead>
		    	<tr>
		      		<th scope="col">No</th>
		      		<th scope="col">Category Name</th>
		      		<th scope="col">Actions</th>
		    	</tr>
		  	</thead>
		  	<tbody>
		   	@forelse( $categories as $category )
		   		<tr>
		   			<th scope="row">{{ $category->id }}</th>
		   			<td>{{ $category->name }}</td>
		   			<td>
		   				<a href="{{ route('categories.edit', $category->id) }}" title="edit" class="btn btn-warning"><i class="nc-icon nc-settings-tool-66"></i></a>
		   				<a href="{{ route('categories.delete', $category->id) }}" title="delete" class="btn btn-danger text-white"><i class="nc-icon nc-simple-remove"></i></a>
		   			</td>
		   		</tr>
		   	@empty
		   	<tr>
		   		<td colspan="3"><h3>No Category!</h3></td>
		   	</tr>
		   	@endforelse
		  </tbody>
		</table>
	</div>
	@endsection