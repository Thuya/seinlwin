<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Shirt'
        ]);
        Category::create([
        	'name' => 'T-shirt'
        ]);
        Category::create([
        	'name' => 'Jacket'
        ]);
        Category::create([
        	'name' => 'Long-sleeved'
        ]);
        Category::create([
        	'name' => 'Hoodie'
        ]);
        Category::create([
        	'name' => 'Sweater'
        ]);
        Category::create([
        	'name' => 'Sweat-Pant'
        ]);
        Category::create([
            'name' => 'Chino-Pant'
        ]);
        Category::create([
            'name' => 'Short'
        ]);
        Category::create([
            'name' => 'Life-style Shoe'
        ]);
        Category::create([
            'name' => 'Sport Shoe'
        ]);
    }
}
