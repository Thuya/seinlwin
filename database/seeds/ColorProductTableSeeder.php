<?php

use Illuminate\Database\Seeder;
use App\ColorProduct;

class ColorProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ColorProduct::create([
        	'product_id' => 1,
        	'color_id' => 1,
        ]);
        ColorProduct::create([
        	'product_id' => 1,
        	'color_id' => 3,
        ]);

        ColorProduct::create([
        	'product_id' => 2,
        	'color_id' => 1,
        ]);
        ColorProduct::create([
        	'product_id' => 2,
        	'color_id' => 3,
        ]);
        ColorProduct::create([
        	'product_id' => 2,
        	'color_id' => 6,
        ]);

        ColorProduct::create([
        	'product_id' => 3,
        	'color_id' => 1,
        ]);
        ColorProduct::create([
        	'product_id' => 3,
        	'color_id' => 6,
        ]);

        ColorProduct::create([
        	'product_id' => 4,
        	'color_id' => 7,
        ]);
        ColorProduct::create([
        	'product_id' => 4,
        	'color_id' => 9,
        ]);

        ColorProduct::create([
        	'product_id' => 5,
        	'color_id' => 2,
        ]);
        ColorProduct::create([
        	'product_id' => 5,
        	'color_id' => 5,
        ]);
        ColorProduct::create([
        	'product_id' => 5,
        	'color_id' => 6,
        ]);

        ColorProduct::create([
            'product_id' => 6,
            'color_id' => 1,
        ]);
        ColorProduct::create([
            'product_id' => 6,
            'color_id' => 4,
        ]);
        ColorProduct::create([
            'product_id' => 6,
            'color_id' => 6,
        ]);
        ColorProduct::create([
            'product_id' => 6,
            'color_id' => 3,
        ]);

        ColorProduct::create([
            'product_id' => 7,
            'color_id' => 4,
        ]);
        ColorProduct::create([
            'product_id' => 7,
            'color_id' => 6,
        ]);
        ColorProduct::create([
            'product_id' => 7,
            'color_id' => 9,
        ]);
    }
}
