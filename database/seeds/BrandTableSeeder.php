<?php

use Illuminate\Database\Seeder;
use App\Brand;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create([
        	'name' => 'Adidas'
        ]);
        Brand::create([
        	'name' => 'Nike'
        ]);
        Brand::create([
        	'name' => 'Polar Skating'
        ]);
        Brand::create([
        	'name' => 'Idea'
        ]);
        Brand::create([
        	'name' => '#FR2'
        ]);
        Brand::create([
        	'name' => 'OFF-WHITE'
        ]);
        Brand::create([
        	'name' => 'Babylon'
        ]);
        Brand::create([
            'name' => 'Baping Ape'
        ]);
        Brand::create([
            'name' => 'Yeezy'
        ]);
        Brand::create([
            'name' => 'Babylon'
        ]);
    }
}
