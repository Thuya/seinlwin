<?php

use Illuminate\Database\Seeder;
use App\ProductImage;

class ProductImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductImage::create([
        	'image' => 'https://media.endclothing.com/media/f_auto,w_1000,h_1000/prodmedia/media/catalog/product/1/7/17-04-2019_champion_reverseweavewomen_slogoscripttee_darkpink_110992-ps114_blr_1.jpg',
        	'product_id' => 1
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201901/54246/thumb/820190124141354-1548315821-cb646190124021341mobile.jpg',
        	'product_id' => 1
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201901/54228/220190124132617-1548312977-bd496190124012617mobile.jpg',
        	'product_id' => 2
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201901/54228/520190124132628-1548312977-bd496190124012617mobile.jpg',
        	'product_id' => 2
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201901/54228/520190124132628-1548312977-bd496190124012617mobile.jpg',
        	'product_id' => 2
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201901/54225/520190124132408-1548312848-wq157190124012408mobile.jpg',
        	'product_id' => 3
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201901/54225/620190124132418-1548312848-wq157190124012408mobile.jpg',
        	'product_id' => 3
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201901/54196/320190124120434-1548308074-jr813190124120434mobile.jpg',
        	'product_id' => 4
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201901/54196/920190124120452-1548308074-jr813190124120434mobile.jpg',
        	'product_id' => 4
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201710/23819/320171027102115-1509076275-vm212171027102115mobile.jpg',
        	'product_id' => 5
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201710/23819/820171027102124-1509076275-vm212171027102115mobile.jpg',
        	'product_id' => 5
        ]);
        ProductImage::create([
        	'image' => 'https://s3.ap-southeast-1.amazonaws.com/barlolo/public/item/201710/23819/820171027102143-1509076275-vm212171027102115mobile.jpg',
        	'product_id' => 5
        ]);

        ProductImage::create([
            'image' => 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/pdtutk8llhuq8rf4hwte/air-max-95-essential-mens-shoe-JPe0Pd.jpg',
            'product_id' => 6
        ]);
        ProductImage::create([
            'image' => 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/d21juvsdmije3bgsqyyn/air-max-95-essential-mens-shoe-JPe0Pd.jpg',
            'product_id' => 6
        ]);
        ProductImage::create([
            'image' => 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/xcmdd5d2uzxvtoihs1si/air-max-95-essential-mens-shoe-JPe0Pd.jpg',
            'product_id' => 6
        ]);
        ProductImage::create([
            'image' => 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/knrforzmtggs2eruunk4/air-max-95-essential-mens-shoe-JPe0Pd.jpg',
            'product_id' => 6
        ]);
        ProductImage::create([
            'image' => 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/vdm5udvmmwqtgilwxxjb/air-vapormax-2019-premium-womens-shoe-kkCRgF.jpg',
            'product_id' => 7
        ]);
        ProductImage::create([
            'image' => 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/qt2x7pyo7jksvdjqwk6t/air-vapormax-2019-womens-shoe-kkCRgF.jpg',
            'product_id' => 7
        ]);
        ProductImage::create([
            'image' => 'https://c.static-nike.com/a/images/t_PDP_1280_v1/f_auto/jb2oz2xhjkwvgkkjtoo8/air-vapormax-2019-womens-shoe-kkCRgF.jpg',
            'product_id' => 7
        ]);
    }
}
