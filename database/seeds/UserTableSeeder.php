<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Admin DTH',
			'email' => 'admin@gmail.com',
			'password'=> Hash::make('admin9876'),
			'role_id' => 1
        ]);

        User::create([
        	'name' => 'Sein Lwin',
			'email' => 'seinlwin@gmail.com',
			'password'=> Hash::make('seinlwin1234'),
			'role_id' => 2
        ]);
    }
}
