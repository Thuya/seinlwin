<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
        	'name' => 'MC Under Armdur Men',
        	'description' => 'Made in China and we have delivery services for the ordered products.',
        	'price' => 198.88,
        	'code' => 'DT-120001',
        	'gender' => 'Male',
            'product_type' => 'shirt',
        	'totalqty' => 70,
        	'category_id' => 1,
        	'brand_id' => 7,
        ]);

        Product::create([
        	'name' => 'Gan Jar Design T Shirt',
        	'description' => 'Made in China and we have delivery services for the ordered products.',
        	'price' => 198.88,
        	'code' => 'DT-120232',
        	'gender' => 'Male',
            'product_type' => 'shirt',
        	'totalqty' => 90,
        	'category_id' => 2,
        	'brand_id' => 7,
        ]);

        Product::create([
        	'name' => 'Cantai Collection Men Shirt',
        	'description' => 'Made in China and we have delivery services for the ordered products.',
        	'price' => 198.88,
        	'code' => 'DT-120432',
        	'gender' => 'Male',
            'product_type' => 'shirt',
        	'totalqty' => 60,
        	'category_id' => 6,
        	'brand_id' => 7,
        ]);

        Product::create([
        	'name' => 'V Shap Girl',
        	'description' => 'Made in China and we have delivery services for the ordered products.',
        	'price' => 108.88,
        	'code' => 'DT-122832',
        	'gender' => 'Female',
            'product_type' => 'shirt',
        	'totalqty' => 60,
        	'category_id' => 2,
        	'brand_id' => 7,
        ]);

        Product::create([
        	'name' => 'Girl Design T-Shirt',
        	'description' => 'Made in China and we have delivery services for the ordered products.',
        	'price' => 198.88,
        	'code' => 'DT-125732',
        	'gender' => 'Female',
            'product_type' => 'shirt',
        	'totalqty' => 90,
        	'category_id' => 4,
        	'brand_id' => 7,
        ]);

        Product::create([
            'name' => 'Nike Air Max 95 Essential',
            'description' => 'The Nike Air Max 95 keeps the classic design lines of the 1995 original and the forefoot Max Air cushioning that made it famous.',
            'price' => 198.88,
            'code' => 'DT-1390902',
            'gender' => 'Male',
            'product_type' => 'shoes',
            'totalqty' => 90,
            'category_id' => 4,
            'brand_id' => 7,
        ]);

        Product::create([
            'name' => 'Nike Air VaporMax ',
            'description' => 'Designed for running but adopted by the street, the Nike Air VaporMax 2019 features the lightest, most flexible Air Max cushioning to-date. A stretch woven material wraps your foot for lightweight support and stability, while an external reinforcement in the heel secures the back of your foot.',
            'price' => 198.88,
            'code' => 'DT-1390902',
            'gender' => 'Female',
            'product_type' => 'shoes',
            'totalqty' => 90,
            'category_id' => 4,
            'brand_id' => 7,
        ]);
    }
}
